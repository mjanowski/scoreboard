<?php

require_once __DIR__.'/vendor/autoload.php';

use Mjanowski\Scoreboard\Scoreboard;

$games = [
    ['homeTeam' => 'Mexico', 'awayTeam' => 'Canada', 'score' => [0,5]],
    ['homeTeam' => 'Spain', 'awayTeam' => 'Brazil', 'score' => [10,2]],
    ['homeTeam' => 'Germany', 'awayTeam' => 'France', 'score' => [2,2]],
    ['homeTeam' => 'Uruguay', 'awayTeam' => 'Italy', 'score' => [6,6]],
    ['homeTeam' => 'Argentina', 'awayTeam' => 'Australia', 'score' => [3,1]],
    ['homeTeam' => 'Poland', 'awayTeam' => 'Russia', 'score' => [3,1]]
];

try {
    $scoreboard = new Scoreboard();

    foreach ($games as $game) {
        $scoreboard->startGame($game['homeTeam'], $game['awayTeam']);
        $scoreboard->updateScore($game['homeTeam'], $game['awayTeam'], $game['score']);
    }

    $scoreboard->finishGame('Poland', 'Russia');

    foreach ($scoreboard->getSummary() as $game) {
        echo $game . ' <br>';
    }

} catch (\Throwable $e) {
    echo sprintf('Error: %s', $e->getMessage());
}
