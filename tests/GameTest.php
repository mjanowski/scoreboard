<?php
namespace Mjanowski\Scoreboard\Tests;

use Mjanowski\Scoreboard\Model\Game;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    public function testIfSetScoreThrowsException(): void
    {
        $this->expectException(\Exception::class);
        $game = new Game('Poland', 'Russia');
        $game->setScore([0]);
    }

    public function testSetScore(): void
    {
        $game = new Game('Poland', 'Russia');
        $score = [10,2];
        $game->setScore($score);
        $this->assertEquals($score, $game->getScore());
    }
}
