<?php
namespace Mjanowski\Scoreboard\Tests;

use Mjanowski\Scoreboard\Model\Game;
use Mjanowski\Scoreboard\Model\GameCollection;
use PHPUnit\Framework\TestCase;

class GameCollectionTest extends TestCase
{
    public function testAddNewGame(): void
    {
        $game = new Game('Poland', 'Russia');
        $gameCollection = new GameCollection();
        $gameCollection->add($game);
        $this->assertEquals($game, $gameCollection[0]);
    }

    public function testExceptionOnDuplicatedGame()
    {
        $this->expectException(\Exception::class);
        $game = new Game('Poland', 'Russia');
        $gameCollection = new GameCollection();
        $gameCollection->add($game);
        $gameCollection->add($game);
        $this->assertEquals($game, $gameCollection[0]);
    }

    public function testDeleteGame(): void
    {
        $game = new Game('Poland', 'Russia');
        $gameCollection = new GameCollection();
        $gameCollection->add($game);
        $gameCollection->delete($game);
        $this->assertCount(0, $gameCollection);
    }

    public function testFindGame(): void
    {
        $game = new Game('Poland', 'Russia');
        $gameCollection = new GameCollection();
        $gameCollection->add($game);
        $gameFound = $gameCollection->find('Poland', 'Russia');
        $this->assertEquals($gameFound, $game);
    }

    public function testSortByScore(): void
    {
        $gameHigher = new Game('Poland', 'Russia');
        $gameHigher->setScore([10,2]);

        $gameLower = new Game('Mexico', 'Canada');
        $gameLower->setScore([2,2]);

        $gameCollection = new GameCollection();
        $gameCollection->add($gameLower);
        $gameCollection->add($gameHigher);

        $gameCollection->sortByScore();

        $this->assertEquals($gameHigher, $gameCollection[0]);
    }
}
