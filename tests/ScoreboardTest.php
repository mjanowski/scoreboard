<?php
namespace Mjanowski\Scoreboard\Tests;

use Mjanowski\Scoreboard\Scoreboard;
use PHPUnit\Framework\TestCase;

class ScoreboardTest extends TestCase
{
    private Scoreboard $scoreboard;
    private string $homeTeam;
    private string $awayTeam;

    private function setupScoreboard(): void
    {
        $this->homeTeam = 'Poland';
        $this->awayTeam = 'Russia';
        $this->scoreboard = new Scoreboard();
        $this->scoreboard->startGame($this->homeTeam, $this->awayTeam);
    }

    public function testStartGame(): void
    {
        $this->setupScoreboard();
        $this->assertCount(1, $this->scoreboard->getSummary());
    }

    public function testFinishGame(): void
    {
        $this->setupScoreboard();
        $this->scoreboard->finishGame($this->homeTeam, $this->awayTeam);
        $this->assertCount(0, $this->scoreboard->getSummary());
    }

    public function testUpdateScore(): void
    {
        $this->setupScoreboard();
        $score = [10, 2];
        $this->scoreboard->updateScore($this->homeTeam, $this->awayTeam, $score);
        $this->assertEquals($score, $this->scoreboard->getSummary()[0]->getScore());
    }
}
