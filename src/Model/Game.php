<?php

namespace Mjanowski\Scoreboard\Model;

use DateTimeInterface;

class Game
{
    private DateTimeInterface $startTime;

    private array $score = [0,0];

    public function __construct(
        private readonly string $homeTeam,
        private readonly string $awayTeam
    )
    {
        $this->startTime = new \DateTime();
    }

    public function getStartTime(): DateTimeInterface
    {
        return $this->startTime;
    }

    public function getHomeTeam(): string
    {
        return $this->homeTeam;
    }

    public function getAwayTeam(): string
    {
        return $this->awayTeam;
    }

    public function getScore(): array
    {
        return $this->score;
    }

    public function setScore(array $score): void
    {
        if (count($score) != 2) {
            throw new \Exception('Invalid score');
        }
        $this->score = $score;
    }

    public function __toString(): string
    {
        return sprintf('%s %d - %s %d', $this->getHomeTeam(), $this->getScore()[0], $this->getAwayTeam(), $this->getScore()[1]);
    }
}
