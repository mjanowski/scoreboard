<?php

namespace Mjanowski\Scoreboard\Model;

class GameCollection implements \Iterator, \Countable, \ArrayAccess
{
    private array $games = [];
    private int $position;

    public function add(Game $game): void
    {
        if ($this->find($game->getHomeTeam(), $game->getAwayTeam())) {
            throw new \Exception('Game already exists. Finish existing game before adding a new one.');
        } else {
            $this->games[] = $game;
        }
    }

    public function delete(Game $gameToDelete): void
    {
        foreach ($this->games as $key => $game) {
            if ($gameToDelete->getHomeTeam() == $game->getHomeTeam() &&
                $gameToDelete->getAwayTeam() == $game->getAwayTeam()) {
                unset($this->games[$key]);
            }
        }
    }

    public function find(string $homeTeam, string $awayTeam): ?Game
    {
        foreach ($this->games as $game) {
            if ($game->getHomeTeam() == $homeTeam && $game->getAwayTeam() == $awayTeam) {
                return $game;
            }
        }
        return null;
    }

    public function sortByScore(): void
    {
        usort($this->games, function ($game1, $game2) {
            $scoreSum1 = $game1->getScore()[0] + $game1->getScore()[1];
            $scoreSum2 = $game2->getScore()[0] + $game2->getScore()[1];
            if ($scoreSum1 != $scoreSum2) {
                return $scoreSum2 <=> $scoreSum1;
            } else {
                return $game2->getStartTime() <=> $game1->getStartTime();
            }
        });
    }

    public function current(): mixed
    {
        return $this->games[$this->position];
    }

    public function next(): void
    {
        ++$this->position;
    }

    public function key(): mixed
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return isset($this->games[$this->position]);
    }

    public function rewind(): void
    {
        $this->position = 0;
    }

    public function count(): int
    {
        return count($this->games);
    }

    public function offsetExists(mixed $offset): bool
    {
        return isset($this->games[$offset]);
    }

    public function offsetGet(mixed $offset): mixed
    {
        return $this->games[$offset] ?? null;
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        if (is_null($offset)) {
            $this->games[] = $value;
        } else {
            $this->games[$offset] = $value;
        }
    }

    public function offsetUnset(mixed $offset): void
    {
        unset($this->games[$offset]);
    }
}
