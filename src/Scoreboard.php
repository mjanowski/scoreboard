<?php

namespace Mjanowski\Scoreboard;

use Mjanowski\Scoreboard\Model\Game;
use Mjanowski\Scoreboard\Model\GameCollection;

class Scoreboard
{
    private GameCollection $gameCollection;

    public function __construct()
    {
        $this->gameCollection = new GameCollection();
    }
    public function startGame(string $homeTeam, string $awayTeam): void
    {
        $this->gameCollection->add(new Game($homeTeam, $awayTeam));
    }

    public function updateScore(string $homeTeam, string $awayTeam, array $score): void
    {
        $game = $this->gameCollection->find($homeTeam, $awayTeam);
        if ($game) {
            $game->setScore($score);
        } else {
            throw new \Exception('Game does not exist');
        }
    }

    public function finishGame(string $homeTeam, string $awayTeam): void
    {
        $game = $this->gameCollection->find($homeTeam, $awayTeam);
        if ($game) {
            $this->gameCollection->delete($game);
        } else {
            throw new \Exception('Game does not exist');
        }
    }

    public function getSummary(): GameCollection
    {
        $this->gameCollection->sortByScore();
        return $this->gameCollection;
    }
}
