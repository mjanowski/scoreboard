# Scoreboard PHP library

Create scoreboard with list of games sorted by score & time
## Prerequirements
To build & run this project you have to make sure that you have `Docker` and `Docker Compose` installed.

## Build & run containers

Build & run containers `docker-compose up`

Example output will be available at `http://localhost:80`

## Usage
Create new instance of scoreboard and start new game:

```
$scoreboard = new Scoreboard();

$scoreboard->startGame('Poland', 'Russia');

// Update score
$score = [10,2]
$scoreboard->updateScore('Poland', 'Russia', $score);

// Finish & delete game from scoreboard
$scoreboard->finishGame('Poland', 'Russia');

// Start more games
$scoreboard->startGame('Germany', 'Russia');
$scoreboard->startGame('Mexico', 'Canada');

//Update scores
$scoreboard->updateScore('Germany', 'Russia', [5,2]);
$scoreboard->updateScore('Mexico', 'Canada', [1,3);

//Display summary, list of games sorted by score
foreach ($scoreboard->getSummary() as $game) {
    echo $game . ' <br>';
}
```

## Tests
Run unit tests: `docker-compose run app 'vendor/bin/phpunit'
`
